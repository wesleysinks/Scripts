#!/bin/bash

#Script to convert m4a to wav audio...

for f in *.m4a
do
  ffmpeg -i "$f" "${f/%m4a/wav}"
done

rm *.m4a