#!/bin/bash

#Script to compress large mp4 into web video in mp4 and webm format...

input="$1"
output=${1%".mp4"}
mkdir Web
ffmpeg -i $input -c:v libx264 -crf 23 -maxrate 1M -bufsize 2M Web/$output.mp4
ffmpeg -i $input -c:v libvpx-vp9 -crf 40 -b:v 2000k Web/$output.webm